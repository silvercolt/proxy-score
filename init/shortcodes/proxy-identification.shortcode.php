<?php

function shortcode_identification( $atts ) {
		
	extract( shortcode_atts( array(
                'email' => null,
        ), $atts ) );

	if( isset($_GET['updated']) ) {
		$updated = true;
	}
	else {
		$updated = false;
	}

	if( !empty($email) ) {
		$email = $email;
		$summary = get_identification( $email, $updated );
	}
    elseif( empty($email) ) {
        $summary = get_identification( null, $updated );
	}
    else {
        $summary = '<div style="color:red">Sorry, no identification could be found for the provided user email. Please ensure a user email is provided.</div>';
    }
        
	return $summary;

}
add_shortcode( 'proxy-my-identification', 'shortcode_identification' );

function get_identification( $email, $updated ) {

	if( empty($email) ) {
		// Get Current User email
		$current_user = wp_get_current_user();
		$email = $current_user->user_email;
	}

	//Set Filters
	$search_criteria['field_filters']['mode'] = 'all';
	$search_criteria['field_filters'][] = array( 'key' => '3', 'value' => $email );
	$sorting = array( 'key' => '3', 'direction' => 'ASC' );
	$paging = array( 'offset' => 0, 'page_size' => 1 );
	$total_count = 0;
	$entries = GFAPI::get_entries( get_option( PS_OPTION_IDENTIFICATION_FORM_ID ), $search_criteria, $sorting, $paging, $total_count );

	if( ! empty($entries) ) {

		$content = '<identification>';
		if($updated) {
			$content .= '<div class="ps-identification-updated">Successfully Updated</div>';
		}
		foreach( $entries as $entry ) {
			$cloudfront_url = ps_cloudfront_url(  rgar( $entry, '2' ) );
			$content .= '<div class="ps-identification-preview"><img src="'. $cloudfront_url .'"/></div>';
			$content .= '<div class="ps-identification-url"><a href="'. $cloudfront_url .'" title="View Identification" target="_blank">View Full Document</a></div>';
		}

		$content .= '</identification>';

	}
	else {
		$content = '<div style="color:red">You don\'t appear to have provided any identification yet.</div>';
	}
    return $content;
    
}