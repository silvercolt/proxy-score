<?php

/*
 * Shortcode to return Counter Boxes of
 * Total Proxy Points in circulation
 * Total Acts of Service Submitted
 */
function shortcode_counters( $atts ) {

	extract( shortcode_atts( array(
		'null' => null,
	), $atts ) );

	//Set Filters
	$search_criteria['field_filters']['mode'] = 'all';
	$acts = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $search_criteria );

	return do_shortcode( '[fusion_counters_box columns="2" color="#1a80b6" title_size="100px" icon="fa-heart" icon_size="50px" icon_top="yes" body_color="#747474" body_size="25px" border_color="rgba(182,70,25,0)" animation_offset="" hide_on_mobile="" class="" id=""][fusion_counter_box value="'. do_shortcode('[mycred_total_points user_id=""]') .'" delimiter="," unit="" unit_pos="" icon="fa-heart"" direction="up"]Points In Circulation[/fusion_counter_box][fusion_counter_box value="'. $acts .'" delimiter="," unit="" unit_pos="" icon="fa-users" direction="up"]Acts of Service[/fusion_counter_box][/fusion_counters_box]' );

}
add_shortcode( 'proxy-counters', 'shortcode_counters' );
