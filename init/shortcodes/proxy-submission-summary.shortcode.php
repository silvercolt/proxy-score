<?php

function shortcode_submission_summary( $atts ) {
		
	extract( shortcode_atts( array(
                'id' => null,
        ), $atts ) );

	if( !empty($id) ) {
            $id = $id;
            $summary = get_submission_summary( $id );
	}
        elseif( !empty($_GET['id']) ) {
            $id = $_GET['id'];
            $summary = get_submission_summary( $id );
	}
        else {
            $summary = '<div style="color:red">Sorry, no service submission could be found. Please ensure an ID is provided.</div>';
        }
        
	return $summary;

}
add_shortcode( 'proxy-submission-summary', 'shortcode_submission_summary' );

function get_submission_summary( $id ) {
    
    $entry = GFAPI::get_entry( $id );
    if( is_a( $entry, 'WP_Error', FALSE ) ) {
        $summary = "Sorry, the summary for submission having ID = " . $id . " could not be found.";
    }
    else {
        $summary .= '<submission id="ps-submission-summary-' . $id . '">';

        $summary .= '<proxy>';
        
        $points_earned_date = rgar( $entry, '23' );
        if(empty($points_earned_date)) {
            $points_earned_date = "Not Yet";
        }
        else {
            $points_earned_date = $points_earned_date;
        }
       
        $summary .= '<div class="ps-submission-secondary-value">Submission ID: ' . $id . '</div>';
            
        $summary .= '<h4>Proxy Details</h4>';

        $summary .= '<div class="ps-submission-points">';
        $summary .= '<div class="ps-submission-points-value">' . rgar( $entry, '20' ) . '</div>';
        $summary .= '<div class="ps-submission-points-label">Point Value</div>';
        $summary .= '<div class="ps-submission-points-earned">Earned: ' . $points_earned_date . '</div>';
        $summary .= '</div>';
        
        $summary .= '<div id="ps-submission-proxy-details">';
        $summary .= '<div id="ps-submission-field-name" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Proxy Name: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . rgar( $entry, '1.3' ) . ' '. rgar( $entry, '1.6' ) . '</span></div>';
        $summary .= '</div>';

        $summary .= '<div id="ps-submission-field-email" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Email: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . rgar( $entry, '2' ) . '</span></div>';
        $summary .= '</div>';
        $summary .= '</div>';

        $summary .= '</proxy>';

        $summary .= '<service>';

        $summary .= '<h4>Service Details</h4>';

         $summary .= '<div id="ps-submission-field-status" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Verification: </span> </div>';
        $summary .= '<div><span class="ps-submission-value"><span class="ps-submission-status ' . strtolower(rgar( $entry, '18' )) . '">' . ucwords(str_replace('_', ' ', rgar( $entry, '18' ))) . '</span> by 3rd-Party,  <span class="ps-submission-status ' . strtolower(rgar( $entry, '19' )) . '">' . ucwords(str_replace('_', ' ', rgar( $entry, '19' ))) . '</span> by Proxy Admin</span></div>';
        $summary .= '</div>';

        $summary .= '<div id="ps-submission-field-date" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Date of Service: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . DateTime::createFromFormat('Y-m-d', rgar( $entry, '3' ))->format('d/m/Y') . '</span></div>';
        $summary .= '</div>';

        $summary .= '<div id="ps-submission-field-act" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Act of Service: </span></div>';
        
        $service_description = PS_POINT_SCHEDULE::SERVICE_ACTS[rgar( $entry, '22' )]['description'];
        if( empty($service_description) ) {
            $service_description = "UNKNOWN";
        }
        
        $summary .= '<div><span class="ps-submission-value">' . $service_description . '</span></div>';
        $summary .= '<div><span class="ps-submission-secondary-value">' . rgar( $entry, '5.1' ) . '</span></div>';
        $summary .= '</div>';

        $years = rgar( $entry, '7' );
        $months = rgar( $entry, '8' );
        $days = rgar( $entry, '9' );
        $hours = rgar( $entry, '10' );
        $hours_per_day = rgar( $entry, '11' );
        $time_spent = null;
        if( !empty($years) ) {
            $time_spent .= $years . ' Years, ';
        }
        if( !empty($months) ) {
            $time_spent .= $months . ' Months, ';
        }
        if( !empty($days) ) {
            $time_spent .= $days . ' Days, ';
        }
        if( !empty($hours) ) {
            $time_spent .= $hours . ' Hours, ';
        }
        if( !empty($hours_per_day) ) {
            $time_spent .= $hours_per_day . ' Hours per day ';
        }
        
        $summary .= '<div id="ps-submission-field-time" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Time Spent: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . $time_spent . '</span></div>';
        $summary .= '</div>';

        $evidence_url = rgar( $entry, '26' );
        $evidence_link = '<a href="' . ps_cloudfront_url( $evidence_url ) . '" title="Click to View" target="_blank">View Available Evidence</a>';
        if( empty($evidence_url) ) {
            $evidence_link = 'No Evidence Provided';
        }
        
        $summary .= '<div id="ps-submission-field-evidence" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Evidence: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . $evidence_link . '</span></div>';
        $summary .= '</div>';

        $summary .= '</service>';

        $summary .= '<verification>';

        $summary .= '<h4>3rd-Party Verification Details</h4>';

        $verifier_first_name = rgar( $entry, '16.3' );
        $verifier_last_name = rgar( $entry, '16.4' );
        if( empty($verifier_first_name) && empty($verifier_last_name) ) {
            $verifier_name = 'Not Available';
        }
        elseif( empty($verifier_first_name) && !empty($verifier_last_name) ) {
            $verifier_name = $verifier_last_name;
        }
        elseif( !empty($verifier_first_name) && empty($verifier_last_name) ) {
            $verifier_name = $verifier_first_name;
        }
        
        $verifier_email = rgar( $entry, '17' );
        if( empty($verifier_email) ) {
            $verifier_email = 'Not Available';
        }
        
        if( filter_var($verifier_email, FILTER_VALIDATE_EMAIL) ) {
            $summary .= '<div class="ps-submission-instructions">';
            $summary .= 'The name and email address of the requested individual who can verify the Act of Service being submitted.  An email has been sent to them with the following link for verification.<br/><br/>Upon successful verification AND provided evidence the proxy points will be awarded, otherwise, a Proxy Score administrator will verify the submission before points are awarded.';
            $summary .= "</div>";
        }
        else {
            $summary .= '<div class="ps-submission-instructions">';
            $summary .= 'Provide the following Link to someone who can verify the Act of Service. This person must have an active ProxyScore Membership.<br/><br/>Upon successful verification AND provided evidence the proxy points will be awarded, otherwise, a Proxy Score administrator will verify the submission before points are awarded.';
            $summary .= "</div>";
        }
        
        if( rgar( $entry, '22' ) == 'SERVICE_PROJECT' ) {
            $summary .= '<div class="ps-submission-instructions special">For the "Organize Service Project" Act of Service, a 3rd-party and Proxy Score Administrator Verification will be required.</div>';
        }
        
        $summary .= '<div id="ps-submission-field-name" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Name: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . $verifier_name . '</span></div>';
        $summary .= '</div>';

        $summary .= '<div id="ps-submission-field-email" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Email: </span></div>';
        $summary .= '<div><span class="ps-submission-value">' . $verifier_email . '</span></div>';
        $summary .= '</div>';
        
        $summary .= '<div id="ps-submission-field-name" class="ps-submission-field">';
        $summary .= '<div><span class="ps-submission-label">Verification Link: </span></div>';
        $summary .= '<div><span class="ps-submission-value"><a href="' . get_site_url() . '/verification?id=' . $id . '" title="3rd Party Verification Link">' . get_site_url() . '/verification?id=' . $id . '</a></span></div>';
        $summary .= '</div>';

        $summary .= '</verification>';

        $summary .= '</submission>';
    }
        
    return $summary;
    
}