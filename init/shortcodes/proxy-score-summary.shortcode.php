<?php

function shortcode_score_summary( $atts ) {
		
	extract( shortcode_atts( array(
                null,
        ), $atts ) );

	$user = wp_get_current_user();
        $summary = get_score_summary( $user );
        
	return $summary;

}
add_shortcode( 'proxy-my-score-summary', 'shortcode_score_summary' );

function get_score_summary( $user ) {
    
    $verified_points = 0;
    $pending_points = 0;
    
    //Set Filters
    $verified_search_criteria['field_filters']['mode'] = 'all';
    $verified_search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $user->user_email );
    $verified_search_criteria['field_filters'][] = array( 'key' => '18', 'value' => 'verified' );
    $verified_search_criteria['field_filters'][] = array( 'key' => '19', 'value' => 'verified' );

    $verified_all_count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_search_criteria );
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    $size = $verified_all_count;
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $verified_all_entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_search_criteria, $sorting, $paging, $total_count );
    foreach($verified_all_entries as $entry){
        $verified_points += rgar( $entry, '20' );
    }
    
    $verified_third_not_search_criteria['field_filters']['mode'] = 'all';
    $verified_third_not_search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $user->user_email );
    $verified_third_not_search_criteria['field_filters'][] = array( 'key' => '18', 'value' => 'verified' );
    $verified_third_not_search_criteria['field_filters'][] = array( 'key' => '19', 'value' => 'not_required' );

    $verified_third_not_count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_third_not_search_criteria );
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    $size = $verified_third_not_count;
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $verified_third_not_entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_third_not_search_criteria, $sorting, $paging, $total_count );
    foreach($verified_third_not_entries as $entry){
        $verified_points += rgar( $entry, '20' );
    }
    
    $verified_admin_not_search_criteria['field_filters']['mode'] = 'all';
    $verified_admin_not_search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $user->user_email );
    $verified_admin_not_search_criteria['field_filters'][] = array( 'key' => '18', 'value' => 'not_required' );
    $verified_admin_not_search_criteria['field_filters'][] = array( 'key' => '19', 'value' => 'verified' );

    $verified_admin_not_count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_admin_not_search_criteria );
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    $size = $verified_admin_not_count;
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $verified_admin_not_entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $verified_admin_not_search_criteria, $sorting, $paging, $total_count );
    foreach($verified_admin_not_entries as $entry){
        $verified_points += rgar( $entry, '20' );
    }
    
    $pending_third_search_criteria['field_filters']['mode'] = 'all';
    $pending_third_search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $user->user_email );
    $pending_third_search_criteria['field_filters'][] = array( 'key' => '18', 'value' => 'pending' );

    $pending_third_count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $pending_third_search_criteria );
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    $size = $pending_third_count;
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $pending_third_entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $pending_third_search_criteria, $sorting, $paging, $total_count );
    foreach($pending_third_entries as $entry){
        $pending_points += rgar( $entry, '20' );
    }
    
    $pending_admin_search_criteria['field_filters']['mode'] = 'all';
    $pending_admin_search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $user->user_email );
    $pending_admin_search_criteria['field_filters'][] = array( 'key' => '19', 'value' => 'pending' );

    $pending_admin_count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $pending_admin_search_criteria );
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    $size = $pending_admin_count;
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $pending_admin_entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $pending_admin_search_criteria, $sorting, $paging, $total_count );
    foreach($pending_admin_entries as $entry){
        $pending_points += rgar( $entry, '20' );
    }
    
    
    $verified_count = $verified_all_count + $verified_third_not_count + $verified_admin_not_count;
    $pending_count = $pending_third_count + $pending_admin_count;

    
    if( !empty($verified_count) || !empty($pending_count) ) {

        $summary .= '<div class="ps-my-submission-summary">';
        
        $summary .= '<div class="ps-my-submission-summary-pending">';
        $summary .= '<div class="ps-submission-points">';
        $summary .= '<div class="ps-submission-points-value">' . $pending_points . '</div>';
        $summary .= '<div class="ps-submission-points-label">Points Pending</div>';
        $summary .= '</div>';
        $summary .= '</div>';
        
        $summary .= '<div class="ps-my-submission-summary-earned">';
        $summary .= '<div class="ps-submission-points">';
        $summary .= '<div class="ps-submission-points-value">' . $verified_points . '</div>';
        $summary .= '<div class="ps-submission-points-label">Points Earned</div>';
        $summary .= '</div>';
        $summary .= '</div>';
        
        $summary .= '</div>';
    
    }
    else {
        $summary = '<div style="color:red; margin-bottom: 25px;">You don\'t appear to have any submissions yet. <a href="/service-submission/">Let\'s Change That!</a></div>';
    }
    
    return $summary;
    
}