<?php

/* 
 * Shortcode to return all of the current user's submissions, option to filter by status
 */

function shortcode_submissions( $atts ) {
		
	extract( shortcode_atts( array(
                'third_status'  => 'all',
                'admin_status'  => 'all',
                'type'          => 'full',
                'size'          => null
        ), $atts ) );

	$submissions = get_submissions( strtolower($third_status), strtolower($admin_status), strtolower($type), $size );
        
	return $submissions;

}
add_shortcode( 'proxy-submissions', 'shortcode_submissions' );

function get_submissions( $third_status, $admin_status, $type, $size ) {
    
    // Get Current User email
    $current_user = wp_get_current_user();
    
    //Set Filters
    $search_criteria['field_filters']['mode'] = 'all';
    $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $current_user->user_email );
    if( $third_status != 'all' ) {
        $search_criteria['field_filters'][] = array( 'key' => '18', 'value' => $third_status );
    }
    if( $admin_status != 'all' ) {
        $search_criteria['field_filters'][] = array( 'key' => '19', 'value' => $admin_status );
    }
    $sorting = array( 'key' => '3', 'direction' => 'ASC' );
    if(empty($size)) {
        $size = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $search_criteria );
    }
    $paging = array( 'offset' => 0, 'page_size' => intval($size) );
    $total_count = 0;
    $entries = GFAPI::get_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $search_criteria, $sorting, $paging, $total_count );
    
    if( !empty($entries) ) {
        
        $submissions .= '<submissions>';
        
        if( $type == 'full' ) {
            $submissions .= '<table id="ps-my-submissions-table" class="table table-striped table-bordered">';
            $submissions .= '<thead>';
            $submissions .= '<tr>';
            $submissions .= '<th>ID</th>';
            $submissions .= '<th>Date</th>';
            $submissions .= '<th>Act of Service</th>';
            $submissions .= '<th>Date of Service</th>';
            $submissions .= '<th>3rd-Party Status</th>';
            $submissions .= '<th>Admin Status</th>';
            $submissions .= '<th>Points</th>';
            $submissions .= '<th>Earned On</th>';
            $submissions .= '</tr>';
            $submissions .= '</thead>';
            $submissions .= '<tbody>';
        }
	    if( $type == 'simple' ) {
		    $submissions .= '<div class="ps-submissions-subtitle">Showing first ' . $size . ' of ' . $total_count . '</div>';
	    }
        foreach( $entries as $entry ) {
            $service_description = PS_POINT_SCHEDULE::SERVICE_ACTS[rgar( $entry, '22' )]['description'];
            $point_value = rgar( $entry, '20' );
            if( empty($service_description) ) {
                $service_description = "UNKNOWN";
            }
            if( empty($point_value) ) {
                $point_value = "unknown";
            }
            if( $type == 'simple' ) {
                $submissions .= '<entry>';
                $submissions .= '<div id="ps-submission-entry-' . $entry['id'] . '" class="ps-submission-entry simple">';
                $submissions .= '<div class="ps-submission-entry-statuses simple">';
                $submissions .= '<a title="3rd Party Status"><div class="ps-submission-entry-status simple ' . strtolower(rgar( $entry, '18' )) . '">3</div></a>';
                $submissions .= '<a title="Proxy Admin Status"><div class="ps-submission-entry-status simple ' . strtolower(rgar( $entry, '19' )) . '">A</div></a>';
                $submissions .= '</div>';
                $submissions .= '<div class="ps-submission-entry-title simple"><a href="submission-confirmation/?id=' .  $entry['id'] . '" title="View Details">' . $service_description . '</a></div>';
                $submissions .= '<div class="ps-submission-entry-date simple">Date of Service: ' . DateTime::createFromFormat('Y-m-d', rgar( $entry, '3' ))->format('m/d/Y') . '</div>';
                $submissions .= '<div class="ps-submission-entry-points simple">Points: ' . $point_value . '</div>';
                $submissions .= '</div>';
                $submissions .= '</entry>';                        
            }
            else {                
                $submissions .= '<tr>';
                $submissions .= '<td>' . $entry['id'] . '</td>';
                $submissions .= '<td>'. DateTime::createFromFormat('Y-m-d H:i:s', $entry['date_created'])->format('n/j/y') .'</td>';
                $submissions .= '<td><a href="/submission-confirmation?id=' . $entry['id'] . '" title="View Details">'. $service_description .'</a></td>';
                $submissions .= '<td>'. DateTime::createFromFormat('Y-m-d', rgar( $entry, '3' ))->format('n/j/y') .'</td>';
                $submissions .= '<td><span class="ps-submission-status ' . strtolower(rgar( $entry, '18' )) . '">' . ucwords(str_replace('_', ' ', rgar( $entry, '18' ))) . '</span></td>';
                $submissions .= '<td><span class="ps-submission-status ' . strtolower(rgar( $entry, '19' )) . '">' . ucwords(str_replace('_', ' ', rgar( $entry, '19' ))) . '</span></td>';
                $submissions .= '<td>'. $point_value .'</td>';
                $submissions .= '<td>'. rgar( $entry, '23' ) .'</td>';
                $submissions .= '</tr>';                
            }
        }
        
        if( $type == 'full' ) {
            $submissions .= '<tbody>';
            $submissions .= '</table>';
        }
        
        $submissions .= '</submissions>';
        
    }
    else {
        $submissions = '<div style="color:red">You don\'t appear to have any submissions yet. <a href="/service-submission/">Let\'s Change That!</a></div>';
    }
    
    return $submissions;
    
}