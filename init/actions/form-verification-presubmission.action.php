<?php

/* 
 * Set the Proxy Name, Email, and Act of Service
 * the verification is for from the Submission Entry ID
 */

function verification_pre_submission( $form ) {
    
    $submission_id = $_POST['input_1'];
    $entry = GFAPI::get_entry($submission_id);
    
    $_POST['input_6'] = rgar( $entry, '1.3' ) . ' ' . rgar( $entry, '1.6' );
    $_POST['input_4'] = rgar( $entry, '2' );
    $_POST['input_5'] = PS_POINT_SCHEDULE::SERVICE_ACTS[rgar( $entry, '22' )]['description'];
    
}
add_action( 'gform_pre_submission_2', 'verification_pre_submission' );

