<?php

/**
 * Utility to replace gravity forms upload URL and send uploaded file to Amazon S3 bucket.
 *
 * @author antonmaju, silvercolt
 * @link https://gist.github.com/antonmaju/eccf799602e95e386670
 *
 */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
require_once WP_CONTENT_DIR . '/plugins/amazon-web-services/vendor/aws/aws-autoloader.php';
use \Aws\S3\S3Client;

// Identification Submission
add_filter('gform_upload_path_4', 'gform_change_upload_path', 10, 2);
add_action('gform_after_submission_4', 'gform_submit_to_s3', 10, 2);

// Service Submission
add_filter('gform_upload_path_1', 'gform_change_upload_path', 10, 2);
add_action('gform_after_submission_1', 'gform_submit_to_s3', 10, 2);

//change gravity form upload URL to point to S3
function gform_change_upload_path($path_info, $form_id)
{
	$bucket_info = get_option('tantan_wordpress_s3');
	$as3cf_is_active = is_plugin_active('amazon-s3-and-cloudfront/wordpress-s3.php');
	if(!empty($bucket_info['bucket']) && $as3cf_is_active )
	{
		$gform_link = explode('gravity_forms/', $path_info["url"]);
		$domain = $bucket_info['cloudfront'];
		$path_info["url"] = "https://".$domain.'/user_data/'.$gform_link[1];
	}
	return $path_info;
}

//submit file to s3
function gform_submit_to_s3($entry, $form)
{
	$bucket_info = get_option('tantan_wordpress_s3');
	$as3cf_is_active = is_plugin_active('amazon-s3-and-cloudfront/wordpress-s3.php');
	if(!empty($form['fields']) && !empty($bucket_info['bucket']) && $as3cf_is_active )
	{
		$s3Client = S3Client::factory(array(
			'key'    => DBI_AWS_ACCESS_KEY_ID,
			'secret' => DBI_AWS_SECRET_ACCESS_KEY,
			'region' => 'us-east-2',
			'version' => 'latest',
			'signature' => 'v4'
		));
		foreach ($form['fields'] as $field)
		{
			if($field->type == 'fileupload' && !empty($entry[$field->id]))
			{
				$gform_link = explode('/user_data/', $entry[$field->id]);
				$upload_dir = wp_upload_dir();
				$file_url = $upload_dir['baseurl'].'/gravity_forms/'.$gform_link[1];
				$url_parts = parse_url( $file_url );
				$full_path = $_SERVER['DOCUMENT_ROOT'] . $url_parts['path'];
				$result = $s3Client->putObject(array(
					'Bucket'       => $bucket_info['bucket'],
					'Key'          => 'user_data/' . $gform_link[1],
					'SourceFile'   => $full_path,
					'ACL'          => 'authenticated-read',
				));

				// Delete original file
				if( !empty($result) ) {
					unlink($full_path);
				}
			}
		}
	}
}