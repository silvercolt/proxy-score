<?php

/* 
 * Add a link to the user's list of service submissions within the Buddy Press
 * Membership profile navigation menu
 */
add_action( 'after_setup_theme', 'ps_register_buddy_profile_menus' );
function ps_register_buddy_profile_menus() {
    register_nav_menu( 'bp-profile-extra-links', __( 'Profile Navigation' ) );
}

add_action( 'bp_member_options_nav', 'ps_add_buddy_profile_menus' );
function ps_add_buddy_profile_menus() {
    
    if ( has_nav_menu( 'bp-profile-extra-links' ) ) {
        wp_nav_menu( array( 'container' => false, 'theme_location' => 'bp-profile-extra-links', 'items_wrap' => '%3$s' ) );
    }
}