<?php

/* 
 * Reward points to user when submission is updated based on changes in the Proxy Admin Status
 */
function ps_update_submission_post_admin_save( $form, $entry_id ) {
    
    $submission_id          = $entry_id;
    $entry                  = GFAPI::get_entry( $entry_id );
    $verification_status    = rgar( $entry, '18' );
    $proxy_admin_status     = rgar( $entry, '19' );
    $points_earned_date     = GFCommon::parse_date( rgar( $entry, '23' ) );
    $act_of_service         = rgar( $entry, '22' );
    $user_id                = rgar( $entry, '24' );
    $point_value            = rgar( $entry, '20' );
    
    // Reward points if verification is "VERIFIED" and Proxy Score Status is NOT REQUIRED or VERIFIED
    if( GFCommon::is_empty_array( $points_earned_date ) && 
            ( $verification_status == 'verified' || $verification_status == 'not_required' ) && 
            ( $proxy_admin_status == 'not_required' || $proxy_admin_status == 'verified' ) ) {

        if( !empty($point_value) || $point_value != 0 ) {
            $rewarded = mycred_add( 'verified_service', $user_id, $point_value, $submission_id . ':' .$act_of_service, $submission_id, $submission_entry );
            if( $rewarded ) {
                // Update Earned Date
                $date = new DateTime();
                $entry['23'] = $date->format('m/d/Y');
                $updated_points_earned_date = GFAPI::update_entry( $entry );
                if( is_a( $updated_points_earned_date, 'WP_Error', TRUE ) ) {
                    error_log( 'PS: Failed to update Submission Points Earned Date for ID = ' . $submission_id, 0);
                }
            }
        }        
        
    }
    
}
add_action( 'gform_after_update_entry_1', 'ps_update_submission_post_admin_save', 10, 2 );
