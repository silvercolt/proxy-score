<?php

/* 
 * Delete any existing Identification entries having the same email address before submitting a new one
 */

function identification_pre_submission( $form ) {

	//Set Filters
	$search_criteria['field_filters']['mode'] = 'all';
	$search_criteria['field_filters'][] = array( 'key' => '3', 'value' => $_POST['input_3'] );
	$sorting = array( 'key' => '3', 'direction' => 'ASC' );
	if(empty($size)) {
		$size = GFAPI::count_entries( get_option( PS_OPTION_IDENTIFICATION_FORM_ID ), $search_criteria );
	}
	$paging = array( 'offset' => 0, 'page_size' => intval($size) );
	$total_count = 0;
	$entries = GFAPI::get_entries( get_option( PS_OPTION_IDENTIFICATION_FORM_ID ), $search_criteria, $sorting, $paging, $total_count );

	if(!empty($entries)) {

		foreach( $entries as $entry ) {

			GFAPI::delete_entry( $entry['id'] );

		}

	}
    
}
add_action( 'gform_pre_submission_4', 'identification_pre_submission' );

