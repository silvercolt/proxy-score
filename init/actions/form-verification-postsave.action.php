<?php

/* 
 * Update a submission post-verification with the verification decision
 */
function ps_update_submission_post_verification( $entry, $form ) {
    
    $verification_status = rgar( $entry, '2' );
    
    //Update the Service Submission's 3rd Party Verification Status
    $submission_id = rgar( $entry, '1' );
    $submission_entry = GFAPI::get_entry( $submission_id );

    $submission_entry['18'] = $verification_status;

    $updated_verification_status = GFAPI::update_entry( $submission_entry );
    if( is_a( $updated_verification_status, 'WP_Error', TRUE ) ) {
        error_log( 'PS: Failed to update Submission Status for ID = ' . $submission_id, 0);
    }
    
    $proxy_admin_status = $submission_entry['19'];
    $points_earned_date = GFCommon::parse_date( $submission_entry['23'] );
    
    // Reward points if verification is "VERIFIED" and Proxy Score Status is NOT REQUIRED or VERIFIED
    if( GFCommon::is_empty_array( $points_earned_date ) && 
            $verification_status == 'verified' && 
            ( $proxy_admin_status == 'not_required' || $proxy_admin_status == 'verified' ) ) {
        
        $act_of_service = $submission_entry['22'];
        $user_id = $submission_entry['24'];
        $point_value = $submission_entry['20'];
        if( !empty($point_value) || $point_value != 0 ) {
            $rewarded = mycred_add( 'verified_service', $user_id, $point_value, $submission_id . ':' .$act_of_service, $submission_id, $submission_entry );
            if( $rewarded ) {
                // Update Earned Date
                $date = new DateTime();
                $submission_entry['23'] = $date->format('m/d/Y');
                $updated_points_earned_date = GFAPI::update_entry( $submission_entry );
                if( is_a( $updated_points_earned_date, 'WP_Error', TRUE ) ) {
                    error_log( 'PS: Failed to update Submission Points Earned Date for ID = ' . $submission_id, 0);
                }
            }
        }        
        
    }
    
}
add_action( 'gform_entry_post_save_2', 'ps_update_submission_post_verification', 10, 2 );
