<?php

add_action('wp_enqueue_scripts', 'ps_enqueue_callback');
function ps_enqueue_callback() {

	$version = PS_VERSION . '.' . wp_rand( 1, 99999999999 );

    wp_enqueue_style( 'proxyscore-styles', PS_PLUGIN_URL . '/assets/styles.css', null, $version );
    wp_enqueue_script( 'proxyscore-js', PS_PLUGIN_URL . '/assets/scripts.js', null, $version );
    
    //Datatables
    //wp_enqueue_style( 'datatables-styles', 'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css', FALSE );
    
    wp_enqueue_style( 'datatables-twitterbootstrap4-styles', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css', null );
    wp_enqueue_style( 'datatables-bootstrap4-styles', 'https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css', null );
    
    wp_enqueue_script( 'datatables-js', 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', null );
    wp_enqueue_script( 'datatables-bootstrap4-js', 'https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js', null );
}

