<?php

add_action( 'plugins_loaded', 'ps_init_plugin', 0 );

function ps_init_plugin() {
    
    require_once PS_PLUGIN_PATH . 'init/actions/enqueue-scripts.php';
    
    load_plugin_textdomain( 'proxyscore', false, PS_PLUGIN_PATH . '/languages' );
    
}