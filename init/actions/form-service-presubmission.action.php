<?php

/* 
 * Capture service entry submission before it is finalized
 * Set Admin Verification Status based on service details
 * Set Point Reward Value based on service details (type and time)
 */

function service_pre_submission( $form ) {
    
    $evidence_field             = $_POST['input_26'];
    $verfication_email_field    = $_POST['input_17'];
    $act_of_service_field       = $_POST['input_22'];
    $years_field                = $_POST['input_7'];
    $months_field               = $_POST['input_8'];
    $days_field                 = $_POST['input_9'];
	$days_per_week_field        = $_POST['input_25'];
    $active_duty_field          = $_POST['input_5.1'];
    
    /*
     * Set Proxy Admin Status NOT REQUIRED
     * IF evidence is NOT provided AND 3rd Party Verification IS provided,
     * THEN Proxy Admin Verification is NOT REQUIRED
     * UNLESS act of service is "Organized service project"
     */
    if( empty($evidence_field) && !empty($verfication_email_field) ) {
        if( $act_of_service_field != 'SERVICE_PROJECT' ) {
            $_POST['input_19'] = 'not_required';
        }
    }
    
    //Retrieve Point Value
    $service_time = array(
        'years'         => $years_field,
        'months'        => $months_field,
        'days'          => $days_field,
        'days_per_week' => $days_per_week_field
    );
    $points = PS_POINT_SCHEDULE::get_points( $act_of_service_field, $service_time );    
    
    // Double-points if Active Military or 'Organized service project' are selected
    if( !empty($active_duty_field) ) {
        $points = intval($points) * 2;
    }
    
    $_POST['input_20'] = $points;
    
}
add_action( 'gform_pre_submission_1', 'service_pre_submission' );
