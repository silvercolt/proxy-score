<?php
function ps_plugin_install()
{
   flush_rewrite_rules();
   if ( is_admin() && 
        current_user_can( 'activate_plugins' ) && 
        (
            !is_plugin_active( 'gravityforms/gravityforms.php' ) || 
            !is_plugin_active( 'mycred/mycred.php' ) ||
            !is_plugin_active( 'amazon-web-services/amazon-web-services.php' ) ||
            !is_plugin_active( 'amazon-s3-and-cloudfront/wordpress-s3.php' )
        ) ) {
            add_action( 'admin_notices', 'ps_required_plugin_notice' );
            deactivate_plugins( PS_PLUGIN_BASENAME );
            if ( isset( $_GET['activate'] ) ) {
                unset( $_GET['activate'] );
            }
   }
   else {
        // check if options exist, if not create defaults
        foreach (PS_SETTINGS::getOptions('general') as $option => $args) {
            add_option( $option, $args['default'] );
         }

         // check if there are required options set, if not advise these options are required
         if( !get_option( PS_OPTION_SERVICE_FORM_ID )  ) {
             add_action( 'admin_notices', 'ps_required_option_service_form_id_notice' );
         }

         if( !get_option( PS_OPTION_VERIFICATION_FORM_ID )  ) {
             add_action( 'admin_notices', 'ps_required_option_service_verification_form_id_notice' );
         }
   }
   
}