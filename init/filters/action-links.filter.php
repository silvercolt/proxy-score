 <?php

/**
* Add relevant links to plugins page
* @param  array $links
* @return array
*/
function ps_plugin_action_links( $links )
{
   $plugin_links = array();
   
   $settings = "<a href='options-general.php?page=proxy-score.php' title='Proxy Score Settings'>Settings</a>";      
   
   array_push( $plugin_links, $settings );
    
   return array_merge( $links, $plugin_links );
   
}
add_filter( 'plugin_action_links_' . PS_PLUGIN_BASENAME, 'ps_plugin_action_links' );