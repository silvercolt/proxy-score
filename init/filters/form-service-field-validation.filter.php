<?php

/* 
 * Validate the 3rd Party Verification fields
 * - No Evidence provided
 * - Specific Acts of Service selected
 * 
 * Validate the Time Spent fields based on Act of Service option
 * - Act of Service Strategy = "day"; require at least 1 Day is submitted
 * - Act of Service Strategy = "hour"; require at least 1 Hour is submitted
 */

add_filter( 'gform_field_validation_1_16', 'ps_verification_name_required', 10, 4 );
add_filter( 'gform_field_validation_1_17', 'ps_verification_email_required', 10, 4 );
add_filter( 'gform_field_validation_1_9', 'ps_time_days_required', 10, 4 );

function ps_verification_name_required( $result, $value, $form, $field  ) {
    
    $fileupload     = rgpost( 'input_26' );
    $actofservice   = rgpost( 'input_23' );    
    
    $first = rgar( $value, $field->id . '.3' );
    $last = rgar( $value, $field->id . '.6' );

    if( empty($fileupload) || $actofservice == 'SERVICE_PROJECT' ) {

       if( empty($first) || empty($last) ) {
           $result = array( 'is_valid' => false, 'message' => 'Please add a 3rd-party Verifier.' );
       }
       else {
           $result = array( 'is_valid' => true, 'message' => '' );
       }

    }
    
    return $result;
    
}

function ps_verification_email_required( $result, $value, $form, $field  ) {
    
    $fileupload     = rgpost( 'input_26' );
    $actofservice   = rgpost( 'input_22' ); 

    if( empty($fileupload) || $actofservice == 'SERVICE_PROJECT' ) {

       if( empty($value) ) {
           $result = array( 'is_valid' => false, 'message' => 'Please add a 3rd-party Verifier email address.' );
       }
       else {
           $result = array( 'is_valid' => true, 'message' => '' );
       }

    }
    
    return $result;
    
}

function ps_time_days_required( $result, $value, $form, $field  ) {
    
    $actofservice   = rgpost( 'input_22' );
    
    $years          = rgpost( 'input_7' );
    $months         = rgpost( 'input_8' );
    $days           = $value;
    
    $service_details = PS_POINT_SCHEDULE::SERVICE_ACTS[$actofservice];

    if( $service_details['point_strategy'] === 'day' ) {

       if( empty($years) && empty($months) && ( empty($days) || $days == '0' ) ) {
           $result = array( 'is_valid' => false, 'message' => $service_details['description'] . ' requires at least 1 ' . $service_details['point_strategy'] );
       }

    }
    
    return $result;
    
}