<?php

/* 
 * Validate on verification submit that the current user is not the same as the submission proxy user
 */

add_filter( 'gform_field_validation_2_2', 'ps_self_verification_check', 10, 4 );

function ps_self_verification_check( $result, $value, $form, $field  ) {
    
    $submission_id = rgpost( 'input_1' );
    $submission_entry = GFAPI::get_entry( $submission_id );
    $proxy_user_id = $submission_entry['24'];
    $current_user_id = get_current_user_id();

    if( $current_user_id === $proxy_user_id ) {
       $result = array( 'is_valid' => false, 'message' => 'Sorry, you cannot verify your own Submission.' );
    }
    else {
       $result = array( 'is_valid' => true, 'message' => '' );
    }
    
    return $result;
    
}