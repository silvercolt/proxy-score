<?php

add_filter( 'gform_pre_render', 'set_conditional_logic' );
add_filter( 'gform_pre_process', 'set_conditional_logic' );
function set_conditional_logic( $form ) {

	//Set conditional logic only for service submission form
	if ( $form['id'] != get_option( PS_OPTION_SERVICE_FORM_ID ) ) {
		return $form;
	}

	// Get Current User email
	$current_user = wp_get_current_user();
	$email = $current_user->user_email;

	//Set Filters
	$search_criteria['field_filters']['mode'] = 'all';
	$search_criteria['field_filters'][] = array( 'key' => '3', 'value' => $email );
	$sorting = array( 'key' => '3', 'direction' => 'ASC' );
	$paging = array( 'offset' => 0, 'page_size' => 1 );
	$total_count = 0;
	$entries = GFAPI::get_entries( get_option( PS_OPTION_IDENTIFICATION_FORM_ID ), $search_criteria, $sorting, $paging, $total_count );

	if( empty($entries) ) {
		$form['fields'] = [];
		$form['button'] = [];
		$form['description'] = 'Please <a href="/acount/my-identification" title="Provide Identification">provide identification</a> before submitting an Act of Service.';
		add_filter( 'gform_submit_button_' . get_option( PS_OPTION_SERVICE_FORM_ID ), '__return_false' );
	}

	return $form;

}