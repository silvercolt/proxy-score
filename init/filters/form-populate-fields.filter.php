<?php

/* 
 * Dynamically populate the list of available Acts of Service to select from
 */

add_filter( 'gform_pre_render_1', 'ps_populate_service_acts' );
add_filter( 'gform_pre_validation_1', 'ps_populate_service_acts' );
add_filter( 'gform_pre_submission_filter_1', 'ps_populate_service_acts' );
add_filter( 'gform_admin_pre_render_1', 'ps_populate_service_acts' );
function ps_populate_service_acts( $form ) {
    
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-acts' ) === false ) {
            continue;
        }

        $acts = PS_POINT_SCHEDULE::SERVICE_ACTS;
 
        $choices = array();
 
        foreach ( $acts as $act => $details ) {
            if(!is_act_restricted($act)) {
                $choices[] = array( 'text' => $details['description'], 'value' => $act );
            }
        }

        $field->placeholder = 'Select an available option';
        $field->choices = $choices;
 
    }
 
    return $form;
    
}

function is_act_restricted($act) {

    $is_restricted = false;
    $is_limted = PS_POINT_SCHEDULE::SERVICE_ACTS[$act]['is_limited'];
    if($is_limted) {
		$current_user_id = get_current_user_id();
		$search_criteria['field_filters']['mode'] = 'all';
		$search_criteria['field_filters'][] = array( 'key' => '24', 'value' => $current_user_id );
		$search_criteria['field_filters'][] = array( 'key' => '22', 'value' => $act );
		$count = GFAPI::count_entries( get_option( PS_OPTION_SERVICE_FORM_ID ), $search_criteria );
		if( $count > 0 ) {
			$is_restricted = true;
		}
    }
    return $is_restricted;

}