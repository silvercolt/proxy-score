<?php


add_filter( 'gform_entry_field_value', function ( $value, $field, $entry, $form ) {
	if ( $field->get_input_type() == 'fileupload' && ! empty( $value ) ) { // Single file upload field
		$file_url = ps_cloudfront_url( rgar( $entry, $field->id ) );
		$value = sprintf( "<a href='%s' target='_blank' title='%s'><img src='%s' width='100' /></a><br>", $file_url, __( 'Click to view', 'gravityforms' ), $file_url );
	}

	return $value;
}, 10, 4 );

add_filter( 'gform_entries_field_value', 'file_upload_field_values', 10, 4 );
function file_upload_field_values( $value, $form_id, $field_id, $entry ) {
	$field = GFAPI::get_field( $form_id, $field_id );
	if ( is_object( $field ) && $field->get_input_type() == 'fileupload' ) {
		$file_path = rgar( $entry, $field_id );
		if ( ! empty( $file_path ) ) {
			$file_path = ps_cloudfront_url( esc_attr( $file_path ) );
			$value     = "<a href='$file_path' target='_blank' title='" . esc_attr__( 'Click to view', 'gravityforms' ) . "'><img src='$file_path' width='100'/></a>";
		}
	}
	return $value;
}