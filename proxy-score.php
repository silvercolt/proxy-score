<?php

/**
 * Plugin Name: Proxy Score
 * Plugin URI: https://proxyscore.net
 * Description: A platform for managing the Proxy Score system, providing shortcodes, integrated functions with Gravity Forms, and MyCRED.  Establishing the scoring protocols and validation of submitted Acts of Service.
 * Author: SilverColt
 * Version: 1.1.0
 * Author URI: https://silvercolt.com
 * Copyright: 2019 ProxyScore.
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: proxy-score
 * Bitbucket Plugin URI: https://bitbucket.org/silvercolt/proxy-score
 * 
*/

/**
 * Verify expected path and functions exist
 */
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/** 
 * --------------------------------------------
 * Plugin Constants 
 * --------------------------------------------
 */
 define( 'PS_VERSION', '1.1.0' );
 define( 'PS_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
 define( 'PS_MAIN_FILE', __FILE__ );
 define( 'PS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
 define( 'PS_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
 define( 'PS_PLUGIN_ACTIVE', true );
 // Default Settings
 define( 'PS_OPTION_GROUP_GENERAL', 'ps_general_options' );
 define( 'PS_OPTION_SERVICE_FORM_ID', 'ps_service_form_id' );
 define( 'PS_OPTION_VERIFICATION_FORM_ID', 'ps_verifications_form_id' );
 define( 'PS_OPTION_IDENTIFICATION_FORM_ID', 'ps_identification_form_id' );
 // amazon S3
 define( 'DBI_AWS_ACCESS_KEY_ID', 'AKIAVUJHA7Z26ZVGZUW7' );
 define( 'DBI_AWS_SECRET_ACCESS_KEY', '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');
 define( 'AS3CF_SETTINGS', serialize( array(
	'provider' => 'aws',
	'access-key-id' => 'AKIAVUJHA7Z26ZVGZUW7',
	'secret-access-key' => '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',
) ) );
 
 /** 
 * --------------------------------------------
 * Include Classes  
 * --------------------------------------------
 */
  require_once 'classes/PS_POINT_SCHEDULE.class.php';
  
  // Autoload Classes
  spl_autoload_register(function ($class_name) {
      if (strpos($class_name, 'PS_') != false) {
          require_once 'classes/' . $class_name . '.class.php';
      }   
  });
  
/** 
 * --------------------------------------------
 * Plugin Activation and Deactivation
 * --------------------------------------------
 */
  require_once 'classes/PS_SETTINGS.class.php';
  require_once 'classes/PS_ADMIN_PAGES.class.php';
  require_once 'init/activation.php';
  add_action( 'admin_init', 'ps_plugin_install' );

/** 
 * --------------------------------------------
 * Included Actions
 * --------------------------------------------
 */
  require_once 'init/actions/pluginloaded.action.php'; // Includes actions and classes to be loaded before the theme
  require_once 'init/actions/form-service-presubmission.action.php';
  require_once 'init/actions/form-verification-presubmission.action.php';
  require_once 'init/actions/form-verification-postsave.action.php';
  require_once 'init/actions/form-service-postsave.action.php';
  require_once 'init/actions/buddypress-member-options.action.php';
  require_once 'init/actions/form-s3-uploader.action.php';
  require_once 'init/actions/form-identification-presubmission.action.php';
 
/** 
 * --------------------------------------------
 * Included Filters
 * --------------------------------------------
 */
 require_once 'init/filters/action-links.filter.php';
 require_once 'init/filters/form-populate-fields.filter.php';
 require_once 'init/filters/form-service-field-validation.filter.php';
 require_once 'init/filters/form-verification-field-validation.filter.php';
 require_once 'init/filters/form-fileupload-entry-field.filter.php';
 require_once 'init/filters/form-service-identification-required.filter.php';
 
 /** 
 * --------------------------------------------
 * Basic Includes
 * --------------------------------------------
 */
 require_once 'inc/ps-cloud-front.php';
 
  /**
  * =============================================
  * Shortcodes
  * =============================================
  */
  require_once 'init/shortcodes/proxy-submission-summary.shortcode.php';
  require_once 'init/shortcodes/proxy-submissions.shortcode.php';
  require_once 'init/shortcodes/proxy-score-summary.shortcode.php';
  require_once 'init/shortcodes/proxy-counter-points.shortcode.php';
  require_once 'init/shortcodes/proxy-identification.shortcode.php';
 
 /**
  * =============================================
  * MISC Functions
  * =============================================
  */
 require_once 'inc/admin-notices.php';