jQuery(document).ready( function ( $ ) {
    $('#ps-my-submissions-table').DataTable( {
        autoWidth: true,
        columns: [
            { width: "7%" },
            null,
            { width: "20%" },
            null,
            { width: "15%" },
            { width: "15%" },
            { width: "12%" },
            null
          ],
        order: [[ 1, 'desc' ]],
        pageLength: 25,
        fixedHeader: true
    } );
} );