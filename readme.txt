=== Starfish Reviews ===
Contributors: silvercolt45
Donate link: https://proxyscore.net
Tags: proxy,score,proxy-score,service,social
Requires at least: 5.0
Tested up to: 5.1.1
Stable tag: 1.1.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A platform for managing the Proxy Score system, providing shortcodes, integrated functions with Gravity Forms, and MyCRED.  Establishing the scoring protocols and validation of submitted Acts of Service.

== Description ==


== Installation ==

* Install and activate Gravity Forms and MyCRED
* Upload Plugin Zip file
* Activate
* Setup General Settings

== Frequently Asked Questions ==



== Changelog ==

= 1.1 – 2019-04-07 =
* Added shortcode for home page counters of total points and acts

= 1.0 – 2019-01-08 =
* Initial development.
