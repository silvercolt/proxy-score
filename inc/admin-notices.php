<?php

/* 
 * Throw an Admin Notice for missing required plugins
 */
function ps_required_plugin_notice() {
    $class = "notice-error";
    $message = __( 'PROXY SCORE: Failed to activate. This plugin requires the following additional plugins to be installed and activated: <strong>Gravity Forms</strong> and <strong>MyCRED</strong>', 'proxyscore' );
    printf( '<div class="notice %1$s"><p>%2$s</p></div>', esc_attr( $class ), $message );
}

function ps_required_option_service_form_id_notice() {
    $class = "notice-warning";
    $message = __( 'PROXY SCORE: The <strong>Service Submission Form ID</strong> is required, visit the <a href="options-general.php?page=proxy-score.php" title="Proxy Score Settings">Settings</a> page to set it now.', 'proxyscore' );
    printf( '<div class="notice is-dismissible %1$s"><p>%2$s</p></div>', esc_attr( $class ), $message );
}

function ps_required_option_service_verification_form_id_notice() {
    $class = "notice-warning";
    $message = __( 'PROXY SCORE: The <strong>Service Verification Form ID</strong> is required, visit the <a href="options-general.php?page=proxy-score.php" title="Proxy Score Settings">Settings</a> page to set it now.', 'proxyscore' );
    printf( '<div class="notice is-dismissible %1$s"><p>%2$s</p></div>', esc_attr( $class ), $message );
}

