<?php

class PS_SETTINGS {

    private $general_options;

    function __construct() {
    
        $this->general_options = self::getOptions('general');
         
        // Register Settings
        add_action( 'admin_init', array( $this, 'register_settings' ) );
        // Add Sections
        add_action( 'admin_init', array( $this, 'section_settings' ) );
            
    }
	
    /**
     * Register Settings: Review Slug
     */
    function register_settings() {
         
        foreach ($this->general_options as $option => $args) {

            register_setting( PS_OPTION_GROUP_GENERAL, $option, $args );
            
        }

     }
	 
    /**
     * Add Setting Sections
     */
    function section_settings() {

           // General Section
           add_settings_section(
               'ps_general_settings_section', // ID
               '', // Title
                array( $this, 'print_general_section_info' ), // Callback
               PS_OPTION_GROUP_GENERAL // Page
           );
           
           // Add Settings Fields by section
           $this->general_settings_fields();

    }
    
    /*
     * General Settings Fields
     */
    function general_settings_fields() {
        
        add_settings_field(
            PS_OPTION_SERVICE_FORM_ID, // ID
            'Service Submission Form ID', // Title 
             array( $this, 'ps_service_form_id_callback' ), // Callback
            PS_OPTION_GROUP_GENERAL, // Page
            'ps_general_settings_section', // Section ID
            array( 
                'label_for' => PS_OPTION_SERVICE_FORM_ID,
                'description' => 'The Gravity Forms ID for the Act of Service Submission form'
            )          
        );  
        
        add_settings_field(
            PS_OPTION_VERIFICATION_FORM_ID, // ID
            'Service Verification Form ID', // Title 
             array( $this, 'ps_service_verification_form_id_callback' ), // Callback
            PS_OPTION_GROUP_GENERAL, // Page
            'ps_general_settings_section', // Section ID
            array( 
                'label_for' => PS_OPTION_VERIFICATION_FORM_ID,
                'description' => 'The Gravity Forms ID for the Service Verification form'
            )          
        );

	    add_settings_field(
		    PS_OPTION_IDENTIFICATION_FORM_ID, // ID
		    'Service Verification Form ID', // Title
		    array( $this, 'ps_identification_form_id_callback' ), // Callback
		    PS_OPTION_GROUP_GENERAL, // Page
		    'ps_general_settings_section', // Section ID
		    array(
			    'label_for' => PS_OPTION_IDENTIFICATION_FORM_ID,
			    'description' => 'The Gravity Forms ID for the Identification form'
		    )
	    );
        
    }
    
    /** 
     * SECTION INFO: Print the General Section text
     */
    function print_general_section_info( $args )
    {
        print "";
    }
	
    /** 
     * FIELD CALLBACK: Service Submission Form ID
     */
    function ps_service_form_id_callback( $args )
    {
        echo '<input name="' . $args['label_for'] . '" type="text" id="' . $args['label_for'] . '" value="' . esc_html(get_option(PS_OPTION_SERVICE_FORM_ID)) . '">';
        echo '<p class="description" id="' . $args['label_for'] . '-description">' . $args['description'] . '</p>';
    }
    
    /** 
     * FIELD CALLBACK: Service Verification Form ID
     */
    function ps_service_verification_form_id_callback( $args )
    {
        echo '<input name="' . $args['label_for'] . '" type="text" id="' . $args['label_for'] . '" value="' . esc_html(get_option(PS_OPTION_VERIFICATION_FORM_ID)) . '">';
        echo '<p class="description" id="' . $args['label_for'] . '-description">' . $args['description'] . '</p>';
    }

	/**
	 * FIELD CALLBACK: Service Verification Form ID
	 */
	function ps_identification_form_id_callback( $args )
	{
		echo '<input name="' . $args['label_for'] . '" type="text" id="' . $args['label_for'] . '" value="' . esc_html(get_option(PS_OPTION_IDENTIFICATION_FORM_ID)) . '">';
		echo '<p class="description" id="' . $args['label_for'] . '-description">' . $args['description'] . '</p>';
	}
    
    
    /**
     * Get Options
     */
    public static function getOptions( $type ) {
        
        switch($type) {
            case 'general':
                return [
                    PS_OPTION_SERVICE_FORM_ID => [
                        'type'              => 'int', 
                        'description'       => 'The Act of Service Submission Form ID', 
                        'sanitize_callback' => null, 
                        'show_in_rest'      => false, 
                        'default'           => ''
                        ],
                    PS_OPTION_VERIFICATION_FORM_ID => [
                        'type'              => 'string', 
                        'description'       => 'The Service Verification Form ID', 
                        'sanitize_callback' => null, 
                        'show_in_rest'      => true, 
                        'default'           => ''
                        ],
                    PS_OPTION_IDENTIFICATION_FORM_ID => [
	                    'type'              => 'string',
	                    'description'       => 'The Identification Form ID',
	                    'sanitize_callback' => null,
	                    'show_in_rest'      => true,
	                    'default'           => ''
                    ]
                ];
                break;
        }
        
    }
}

if( is_admin() ) {
    if (class_exists('PS_SETTINGS', true)) {
        return new PS_SETTINGS;
    }
}
