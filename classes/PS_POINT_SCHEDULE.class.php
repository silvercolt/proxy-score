<?php

class PS_POINT_SCHEDULE {
    
    const SERVICE_ACTS = array(
        'STUCK_ROAD' => array(
            'description'       => 'Help someone stuck on the side of the road',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 2,
            'is_limited'        => false
        ),
        'FOOD_BANK' => array(
            'description'       => 'Help at the a food bank or food line',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 1,
            'is_limited'        => false
        ),
        'CHURCH_SERVICES' => array(
            'description'       => 'Church service: teaching, mission, church service project',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 1,
            'is_limited'        => false
        ),
        'EAGLE_SCOUT' => array(
            'description'       => 'Eagle scout project',
            'point_strategy'    => 'base',
            'base_points'       => 100,
            'points_per'        => 0,
            'is_limited'        => true
        ),
        'SERVICE_PROJECT' => array(
            'description'       => 'Organized service project',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 5,
            'is_limited'        => false
        ),
        'MILITARY_SERVICE' => array(
            'description'       => 'Military service (non-wartime)',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 2,
            'is_limited'        => false
        ),
        'DONATED_BLOOD' => array(
            'description'       => 'Donated blood or plazma',
            'point_strategy'    => 'base',
            'base_points'       => 3,
            'points_per'        => 0,
            'is_limited'        => false
        ),
        'VOLUNTEER_HOSPITAL' => array(
            'description'       => 'Volunteer at a hospital',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 1,
            'is_limited'        => false
        ),
        'VOLUNTEER_SCHOOL' => array(
            'description'       => 'Volunteer at a school',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 1,
            'is_limited'        => false
        ),
        'VOLUNTEER_COACH' => array(
            'description'       => 'Volunteer coach',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 1,
            'is_limited'        => false
        ),
        'POLICE_OFFICER' => array(
            'description'       => 'Police Officer',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 2,
            'is_limited'        => false
        ),
        'MILITARY_SERVICE_VETERAN' => array(
            'description'       => 'Military service in war time (Veteran)',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 5,
            'is_limited'        => false
        ),
        'PEACE_CORE' => array(
            'description'       => 'Peace Core',
            'point_strategy'    => 'day',
            'base_points'       => 0,
            'points_per'        => 2,
            'is_limited'        => false
        ),
        'WRITE_LETTER' => array(
            'description'       => 'Write a heart felt letter of appreciation to someone',
            'point_strategy'    => 'base',
            'base_points'       => 5,
            'points_per'        => 0,
            'is_limited'        => false
        ),
        'HIGHSCHOOL_PROJECT' => array(
            'description'       => 'High school Senior Project',
            'point_strategy'    => 'base',
            'base_points'       => 75,
            'points_per'        => 0,
            'is_limited'        => true
        )
    );

    public static function get_points( $act_of_service, $service_time ) {
        
        $service_years          = intval($service_time['years']);
        $service_months         = intval($service_time['months']);
        $service_days           = intval($service_time['days']);
	    $service_days_per_weeks = intval($service_time['days_per_week']);
        
        $strategy               = self::SERVICE_ACTS[$act_of_service]['point_strategy'];
        $base_points            = self::SERVICE_ACTS[$act_of_service]['base_points'];
        $points_per             = self::SERVICE_ACTS[$act_of_service]['points_per'];
        $total_points           = $base_points;
        
        if( $strategy === 'day' ) {
            $days = ($service_years * 52 * $service_days_per_weeks);
            $days += ($service_months * 4 * $service_days_per_weeks);
            $days += $service_days;
            $total_points += ($days * $points_per);
        }
        
        return $total_points;
        
    }
    
}

if( is_admin() ) {
    if (class_exists('PS_POINT_SCHEDULE', true)) {
        return new PS_POINT_SCHEDULE;
    }
}

