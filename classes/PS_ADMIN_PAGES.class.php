<?php
     
class PS_ADMIN_PAGES {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
    }
        
    function admin_menu() {
        add_options_page( 
            esc_html__( 'Proxy Score', 'proxyscore' ), // Page Title
            esc_html__( 'Proxy Score', 'proxyscore' ), // Menu Title
            'manage_options', // Capability required
            'proxy-score.php', // Menu Slug
            array(
                $this,
                'ps_options_page'
            )
        ); 
    }

    function ps_options_page() {

       ?>
       <div class="wrap">
           <h1><?php _e( 'Proxy Score Settings', 'starfish' ); ?></h1>
           <?php settings_errors(); ?> 
           <div id="description"></div>
           <?php $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_options'; ?>
           <h2 class="nav-tab-wrapper">
                <a href="proxy-score.php&tab=general_options" class="nav-tab <?php echo $active_tab == 'general_options' ? 'nav-tab-active' : ''; ?>">General</a>
           </h2>
           <form method="post" action="options.php">
           <?php
               // This prints out all hidden setting fields
                if( $active_tab == 'general_options' ) {
                     settings_fields( PS_OPTION_GROUP_GENERAL );
                     do_settings_sections( PS_OPTION_GROUP_GENERAL );
                 } else {

                 } // end if/else
               submit_button();
           ?>
           </form>
       </div>
       <?php

    }
}

if( is_admin() ) {
    if (class_exists('PS_ADMIN_PAGES', true)) {
        return new PS_ADMIN_PAGES;
    }
}
